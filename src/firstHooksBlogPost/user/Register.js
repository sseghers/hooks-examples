import React, { useState } from 'react';

import {handleInput, handleSubmit} from '../inputHelpers';

function Register({
  registerUser,
}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');

  const disabled = !Boolean(username)
    || !Boolean(password.length)
    || password !== passwordRepeat;

  return (
    <form onSubmit={handleSubmit(registerUser.bind(this, username))}>
      <label htmlFor="register-username">Username:</label>
      <input
        onChange={handleInput(setUsername)}
        id="register-username"
        placeholder="Cool User"
        type="text"
        value={username}
      />
      <label htmlFor="register-password">Password:</label>
      <input
        onChange={handleInput(setPassword)}
        id="register-password"
        placeholder="secret words"
        type="text"
        value={password}
      />
      <label htmlFor="register-password-repeat">Repeat password:</label>
      <input
        onChange={handleInput(setPasswordRepeat)}
        id="register-password-repeat"
        placeholder="secret words"
        type="text"
        value={passwordRepeat}
      />
      <input
        disabled={disabled}
        type="submit"
        value="Register"
      />
    </form>
  )
}

export {Register}
