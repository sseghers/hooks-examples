import React, { useState } from 'react';

import {handleInput, handleSubmit} from '../inputHelpers';

function Login({
  loginUser,
}) {
  const [username, setUsername] = useState('');

  return (
    <form onSubmit={handleSubmit(loginUser.bind(this, username))}>
      <label htmlFor="login-username">Username:</label>
      <input
        onChange={handleInput(setUsername)}
        id="login-username"
        placeholder="Cool User"
        type="text"
        value={username}
      />
      <label htmlFor="login-password">Password:</label>
      <input
        id="login-password"
        placeholder="secret words"
        type="text"
      />
      <input
        disabled={!Boolean(username)}
        type="submit"
        value="Login"
      />
    </form>
  )
}

export {Login}
