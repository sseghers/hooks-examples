import React from 'react';

import {handleSubmit} from '../inputHelpers';

function Logout({
  setUser,
  user,
}) {
  return (
    <form onSubmit={handleSubmit(setUser.bind(this, ''))}>
      <label htmlFor="logout-submit">Logged in as: <strong>{user}</strong> </label>
      <input
        id="logout-submit"
        type="submit"
        value="Logout"
      />
    </form>
  )
}

export {Logout}
