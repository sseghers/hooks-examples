export const handleSubmit = (handler = () => null) => event => {
  event.preventDefault();
  handler();
}

export const handleInput = handler => event => handler(event.target?.value);
