import React, { useState } from 'react';

import {PostList} from './post/PostList';
import {CreatePost} from './post/CreatePost';
import {UserBar} from './user/UserBar';

const defaultPosts = [
  {
    author: 'Admin',
    content: 'First lol',
    id: 'uID1',
    title: 'First'
  },
  {
    author: 'Anon',
    content: 'Test Message',
    id: 'uID2',
    title: 'Foo'
  },
  {
    author: 'Super User',
    content: 'Super Message',
    id: 'uID3',
    title: 'Super Duper'
  }
];

const updatePosts = (handler, posts) =>
  post => handler([{...post, id: `uID${posts.length + 1}`}, ...posts]);

function App() {
  const [user, setUser] = useState('');
  const [posts, setPosts] = useState(defaultPosts);
  
  const createPost = user && <CreatePost
    setPosts={updatePosts(setPosts, posts)}
    user={user} />;

  return (
    <div style={{padding: 8}}>
      <UserBar user={user} setUser={setUser} />
      <br />
      {createPost}
      <br />
      <hr />
      <PostList posts={posts} />
    </div>
  )
}

export {App}
