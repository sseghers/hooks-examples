import React, { useState } from 'react';

import { handleInput, handleSubmit } from '../inputHelpers';

function makePost(
  content,
  setPosts,
  title,
  user,
) {
  return () => setPosts({
    author: user,
    content,
    title,
  })
}

function CreatePost({
  setPosts,
  user,
}) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  return (
    <form onSubmit={handleSubmit(makePost(content, setPosts, title, user))}>
      <div>Author: <strong>{user}</strong></div>
      <br />
      <div>
        <label htmlFor="createPost-title">Title: </label>
        <input
          onChange={handleInput(setTitle)}
          id="createPost-title"
          type="text"
          value={title}
        />
      </div>
      <br />
      <textarea onChange={handleInput(setContent)} value={content} />
      <br />
      <input
        type="submit"
        value="Create"
        />
    </form>
  )
}

export {CreatePost}
