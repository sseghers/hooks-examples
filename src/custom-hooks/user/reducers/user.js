import * as userTypes from '../actions/types';

const initialState = {
  password: '',
  username: '',
};

function userReducer(state = initialState, action) {
  switch(action.type) {
    case userTypes.LOGIN:
    case userTypes.REGISTER:
      return {
        ...action.payload
      };
    case userTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
}

export {userReducer}
