import React, { useContext, useEffect } from 'react';
import { useResource } from 'react-request-hook';
import {useInput} from 'react-hookedup';

import {StateContext} from '../contexts';
import {register} from './actions/user';
import {handleSubmit} from '../inputHelpers';

function Register() {
  const {
    bindToInput: bindUsername,
    value: username,
  } = useInput('');
  const {
    bindToInput: bindPassword,
    value: password
  } = useInput('');
  const {
    bindToInput: bindPasswordRepeat,
    value: passwordRepeat
  } = useInput('');
  const {dispatch} = useContext(StateContext);

  const [newUser, postUser] = useResource(user => ({
    url: '/users',
    method: 'post',
    data: user,
  }));
  useEffect(() => {
    if(newUser && newUser.data) {
      dispatch(register({username, password}));
    }
  }, [newUser]);

  const createUser = () => {
    postUser({username, password});
  }
  

  const disabled = !Boolean(username)
    || !Boolean(password.length)
    || password !== passwordRepeat;

  return (
    <form onSubmit={handleSubmit(createUser)}>
      <label for="register-username">Username:</label>
      <input
        {...bindUsername}
        id="register-username"
        placeholder="Cool User"
        type="text"
        value={username}
      />
      <label for="register-password">Password:</label>
      <input
        {...bindPassword}
        id="register-password"
        placeholder="secret words"
        type="text"
        value={password}
      />
      <label for="register-password-repeat">Repeat password:</label>
      <input
        {...bindPasswordRepeat}
        id="register-password-repeat"
        placeholder="secret words"
        type="text"
        value={passwordRepeat}
      />
      <input
        disabled={disabled}
        type="submit"
        value="Register"
      />
    </form>
  )
}

export {Register}
