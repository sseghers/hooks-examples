import React from 'react';
import {Link} from 'react-navi';

import {useTheme} from '../hooks/useTheme';

const Post = React.memo(function ({
  author,
  content,
  id,
  short = false,
  title,
}) {
  const {secondaryColor} = useTheme();

  let renderedContent = content;
  let maybeContentLink = null;
  if (short) {
    const slicedContent = content.slice(0, 30);
    
    if(slicedContent.length === 30) {
      renderedContent = `${slicedContent}...`
      maybeContentLink = (<div>
        <br />
        <Link href={`/view/${id}`}>View full post</Link>
      </div>)
    }
  }

  return (
    <div>
      <h3 style={{color: secondaryColor}}>{title}</h3>
      <div>{renderedContent}</div>
      {maybeContentLink}
      <br />
      <em>Written by: <strong>{author}</strong></em>
    </div>
  )
})

export { Post }
