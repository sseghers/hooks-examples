import React, { Fragment, useContext } from 'react';

import {StateContext} from '../contexts';
import {getPostsState} from './selectors/selectors';
import {Post} from './Post';

function PostList() {
  const {state} = useContext(StateContext);
  const posts = getPostsState(state);
  return (
    <div>
      {posts.map(post => (
        <Fragment key={post.id}>
          <Post
            {...post}
            key={post.id}
            short
          />
          <hr />
        </Fragment>
        ))
      }
    </div>
  )
}

export { PostList }
