import React, { useEffect, useContext } from 'react';
import { useResource } from 'react-request-hook';

import {PostList} from '../post/PostList';
import {getErrorsState} from '../error/selectors/selectors';
import {StateContext} from '../contexts';
import {capturePostError} from '../error/actions/errors';
import {retrieve} from '../post/actions/post';

function HomePage() {
  const {state, dispatch} = useContext(StateContext);
  /* Fetch posts */
  const [posts, getPosts] = useResource(() => ({
    url: '/posts',
    method: 'get',
  }));
  useEffect(getPosts, []);
  useEffect(() => {
    if(posts && posts.error) {
      dispatch(capturePostError(posts.error));
    }
    if(posts && posts.data) {
      dispatch(retrieve(posts.data.reverse()));
    }
  }, [posts]);
  
  const error = getErrorsState(state);

  
  let postsBlock = null;
  if(error) {
    postsBlock = <strong>{error}</strong>
  } else {
    postsBlock = <PostList />
  }

  return (
    <div>{postsBlock}</div>
  )
}

export {HomePage}
