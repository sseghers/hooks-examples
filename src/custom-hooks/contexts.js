import React from 'react';

export const BlogThemeContext = React.createContext({
  primaryColor: 'deepskyblue',
  secondaryColor: 'coral',
});

export const StateContext = React.createContext({
  dispatch: () => null,
  state: {},
});
