import {useState, useCallback} from 'react';

function useCounter(initialState = 0) {
  const [count, setCount] = useState(initialState);

  const increment = useCallback(() => setCount(count + 1), [count]);
  const reset = useCallback(() => setCount(initialState), [initialState]);

  return {
    count,
    increment,
    reset,
  };
}

export {useCounter}
