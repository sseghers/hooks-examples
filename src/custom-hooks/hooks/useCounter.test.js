import { renderHook, act } from '@testing-library/react-hooks';

import {useCounter} from './useCounter';

describe('useCounter', () => {
  it('should use counter', () => {
    const expectedInitialState = 42;
    
    const { result } = renderHook(() => useCounter(expectedInitialState));

    expect(result.current.count).toBe(expectedInitialState);
    expect(typeof result.current.increment).toBe('function');
  });

  it('should increment counter', () => {
    const expectedInitialState = 0;
    
    const { result } = renderHook(() => useCounter(expectedInitialState));
    act(() => result.current.increment());

    expect(result.current.count).toBe(1);
  });

  it('should use initial value', () => {
    const { result } = renderHook(() => useCounter(123));

    const expectedInitialValue = result.current.count;
    act(() => result.current.increment());
    
    expect(expectedInitialValue).toBe(123);
    expect(result.current.count).toBe(124);
  });

  it('should reset to initial value', () => {
    const {result, rerender} = renderHook((initialState) => useCounter(initialState), {
      initialProps: 0
    });
    rerender(123);
    act(() => result.current.reset());
    
    expect(result.current.count).toBe(123);
  });
});
