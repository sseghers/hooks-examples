import {useContext} from 'react';

import {BlogThemeContext} from '../contexts';

export const useTheme = () => useContext(BlogThemeContext);
