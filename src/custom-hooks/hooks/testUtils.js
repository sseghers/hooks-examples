import React from 'react';

import {BlogThemeContext} from '../contexts';

function ThemeContextWrapper({children}) {
  return (
    <BlogThemeContext.Provider value={{
      primaryColor: 'deepskyblue',
      secondaryColor: 'coral',
    }}>{children}</BlogThemeContext.Provider>
  );
}

export {ThemeContextWrapper}
