import React from "react";
import ReactDOM from 'react-dom';

const log = console.log;

let values = [];
let currentHook = 0;

function useState(initialValue) {
  let hookIndex = currentHook;
  if(values[currentHook] === undefined) {
    values[currentHook] = initialValue
  }
  
  function setValue(newValue) {
    values[hookIndex] = newValue;
    log(values);
    ReactDOM.render(
      <App />,
      document.getElementById("root")
    );
  }
  return [values[currentHook++], setValue];
}

const handleInput = handler => event => handler(event.target.value); 

export default function App() {
  currentHook = 0;
  const [messageOne, setMessageOne] = useState('foo bar');
  const [messageTwo, setMessageTwo] = useState('foo bar');
  const handleTextOne = handleInput(setMessageOne);
  const handleTextTwo = handleInput(setMessageTwo);

  return (
    <div className="App">
      <span label-for="firstMessage">First Message</span>
      <input
        id="firstMessage"
        value={messageOne}
        onChange={handleTextOne}
        type="text" />
      <br />
      <span label-for="secondMessage">Second Message</span>
      <input
        id="secondMessage"
        value={messageTwo}
        onChange={handleTextTwo}
        type="text" />
    </div>
  );
}
