import React from 'react';

import {ThemeContext} from './appContext';
import {Header} from './Header';

function App() {
  return (
    <ThemeContext.Provider value={{primaryColor: 'coral'}}>
      <Header text="Hello World" />
      <ThemeContext.Provider value={{primaryColor: 'red'}}>
        <Header text="Second Header test" />
      </ThemeContext.Provider>
    </ThemeContext.Provider>
  );
}

export {App};
