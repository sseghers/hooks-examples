import React from 'react';

const defaultTheme = {
  primaryColor: 'deepskyblue',
};

export const ThemeContext = React.createContext(defaultTheme);
