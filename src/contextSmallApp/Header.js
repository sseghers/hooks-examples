import React, {useContext} from 'react';
import {ThemeContext} from './appContext';

function Header({
  text,
}) {
  const theme = useContext(ThemeContext);
  return (<h1 style={{color: theme.primaryColor}}>{text}</h1>)
}

export {Header}
