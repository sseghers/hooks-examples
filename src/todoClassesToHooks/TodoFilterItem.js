import React from 'react';

function TodoFilterItem({
  filter = 'all',
  filterTodos,
  name
}) {

  function handleFilter() {
    filterTodos(name);
  }

  const filterStyles = {
    color: 'blue',
    cursor: 'pointer',
    fontWeight: (filter === 'name' ? 'bold' : 'normal')
  }

  return <span
    style={filterStyles}
    onClick={handleFilter}
    >{name}</span>; 
}

export {TodoFilterItem}
