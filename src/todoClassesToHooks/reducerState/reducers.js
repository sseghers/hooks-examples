import {generateUID} from '../api';
import {
  ADD_TODO, 
  FETCH_TODOS,
  FILTER_TODOS,
  REMOVE_TODO, 
  TOGGLE_TODO, 
} from './actions';


const filterInitialState = {
  filter: '',
}
const todosInitialState = {
  todos: [],
}

export const rootInitialState = {
  filter: {
    ...filterInitialState.filter,
  },
  todos: [
    ...todosInitialState.todos,
  ],
}

function filterReducer(state = filterInitialState, action) {
  switch(action.type) {
    case FILTER_TODOS:
      return action.payload.filter;
    default:
      return state;
  }
}

function todosReducer(state = todosInitialState, action) {
  switch(action.type) {
    case ADD_TODO:
      return [
        {
          ...action.payload,
          id: generateUID(),
        },
        ...state,
      ];
    case FETCH_TODOS:
      return [...action.payload.todos];
    case REMOVE_TODO:
      return state.filter(todo => todo.id !== action.payload.id);
    case TOGGLE_TODO:
      return state.map(todo => {
        if(todo.id === action.payload.id) {
          return {
            ...todo,
            completed: !todo.completed,
          }
        }
        return todo;
      });
    default:
      return state;
  }
}

export function rootReducer(state = rootInitialState, action) {
  return {
    filter: filterReducer(state.filter, action),
    todos: todosReducer(state.todos, action),
  };
} 
