export const FETCH_TODOS = 'FETCH_TODOS';
export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const FILTER_TODOS = 'FILTER_TODOS';

export const fetchTodos = todos => {
  return {
    type: FETCH_TODOS,
    payload: {
      todos,
    }
  };
};

export const addTodo = title => ({
  type: ADD_TODO,
  payload: {
    completed: 'false',
    title,
  }
});

export const toggleTodo = id => ({
  type: TOGGLE_TODO,
  payload: {
    id,
  }
});

export const removeTodo = id => ({
  type: REMOVE_TODO,
  payload: {
    id
  }
});

export const filterTodos = filter => ({
  type: FILTER_TODOS,
  payload: {
    filter,
  }
});
