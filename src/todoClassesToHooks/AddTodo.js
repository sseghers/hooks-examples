import React, {useState} from 'react';

import {handleInput} from './helpers';

function AddTodo({
  addTodo,
}) {
  const [input, setInput] = useState('');
  function handleAdd() {
    if(input) {
      addTodo(input);
     setInput('');
    }
  }
  return (
    <form onSubmit={event => {
      event.preventDefault();
      handleAdd();
    }}>
      <input
        onChange={handleInput(setInput)}
        placeholder="Enter new task..."
        style={{width: 350, height: 15}}
        type="text"
        value={input}
      />
      <input
        disabled={!input}
        style={{float: 'right', marginTop: 2}}
        type="submit"
        value="Add"
      />
    </form>
  );
}

export {AddTodo}
