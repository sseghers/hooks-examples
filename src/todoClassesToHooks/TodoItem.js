import React from 'react';

function TodoItem({
  completed,
  id,
  removeTodo,
  title,
  toggleTodo,
}) {
  function handleRemove() {
    removeTodo(id);
  }

  function handleToggle() {
    toggleTodo(id);
  }

  return (
    <div style={{width: 400, height: 25}}>
      <input
        checked={Boolean(completed)}
        onChange={handleToggle}
        type="checkbox"
      />
      {title}
      <button
        style={{float: 'right'}}
        onClick={handleRemove}
        >X</button>
    </div>
  );
}

export {TodoItem}
