export const handleInput = handler => event => handler(event.target?.value);
