import React from 'react';

import {TodoFilterItem} from './TodoFilterItem';

function TodoFilter(props) {
  return (
    <div>
      <TodoFilterItem name="all" {...props} />{'  /  '}
      <TodoFilterItem name="active" {...props} />{'  /  '}
      <TodoFilterItem name="completed" {...props} />
    </div>
  )
}

export {TodoFilter}
