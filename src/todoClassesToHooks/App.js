import React, {useReducer, useEffect, useMemo} from 'react';

import {AddTodo} from './AddTodo';
import {Header} from './Header';
import {StateContext} from './StateContext';
import {TodoFilter} from './TodoFilter';
import {TodoList} from './TodoList';
import {fetchAPITodos} from './api';
import {rootReducer, rootInitialState} from './reducerState/reducers';
import {
  addTodo,
  fetchTodos,
  filterTodos,
  removeTodo,
  toggleTodo,
} from './reducerState/actions';
import './App.css';

function App() {
  const [state, dispatch] = useReducer(rootReducer, rootInitialState)

  useEffect(() => {
    fetchAPITodos().then(data => dispatch(fetchTodos(data)));
  }, []);

  const filteredTodos = useMemo(() => {
    const { filter, todos } = state;
    switch(filter) {
      case 'active':
          return todos.filter(todo => !todo.completed);
      case 'completed':
        return todos.filter(todo => todo.completed);
      case 'all':
      default:
        return todos;
    }
  }, [state]);

  return (
    <StateContext.Provider value={filteredTodos}>
      <div style={{ width: 400 }}>
        <Header />
        <AddTodo addTodo={title => dispatch(addTodo(title))} />
        <hr />
        <TodoList
          toggleTodo={id => dispatch(toggleTodo(id))}
          removeTodo={id => dispatch(removeTodo(id))}
        />
        <hr />
        <TodoFilter
          filter={state.filter}
          filterTodos={filter => dispatch(filterTodos(filter))}
        />
      </div>
    </StateContext.Provider>
  );
}

export {App}
