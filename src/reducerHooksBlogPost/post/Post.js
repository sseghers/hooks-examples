import React from 'react';

function Post({
  author,
  content,
  title,
}) {
  return (
    <div>
      <h3>{title}</h3>
      <div>{content}</div>
      <br />
      <em>Written by: <strong>{author}</strong></em>
    </div>
  )
}

export {Post}
