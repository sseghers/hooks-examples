import * as postTypes from './types';

/**
 * {
 *  author: '',
 *  content: '',
 *  id: '',
 *  title: '',
 * }
 */
export const create = payload => ({
  payload,
  type: postTypes.create,
})
