import React, { Fragment } from 'react';

import {Post} from './Post';

function PostList({
  posts
}) {
  return (
    <div>
      {posts.map(post => (
        <Fragment key={post.id}>
          <Post
          {...post}
          key={post.id} />
          <hr />
        </Fragment>
        ))
      }
    </div>
  )
}

PostList.defaultProps = {
  posts: []
}

export { PostList }
