import React from 'react';

import {Login} from './Login';
import {Logout} from './Logout';
import {Register} from './Register';

function UserBar({
  setUser,
  user,
}) {
  if(user) {
    return <Logout
      user={user}
      setUser={setUser}
      />
  }

  return (
  <>
    <Login loginUser={setUser} />
    <br />
    <Register registerUser={setUser} />
  </>
  )
}

export {UserBar}
