import * as userTypes from '../actions/types';

const initialState = '';

function userReducer(state = initialState, action) {
  switch(action.type) {
    case userTypes.LOGIN:
    case userTypes.REGISTER:
      return action.payload.username;
    case userTypes.LOGOUT:
      return '';
    default:
      return state;
  }
}

export {userReducer}
