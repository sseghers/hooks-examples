import * as userTypes from './types';

/** 
 * {
 *  username: '',
 *  password: '',
 * }
 */
export const login = payload => ({
  payload,
  type: userTypes.LOGIN,
});

/** 
 * {
 *  username: '',
 *  password: '',
 *  passwordRepeat: '',
 * }
 */
export const register = payload => ({
  payload,
  type: userTypes.REGISTER,
});

/** 
 * {}
 */
export const logout = () => ({
  type: userTypes.LOGOUT,
});
