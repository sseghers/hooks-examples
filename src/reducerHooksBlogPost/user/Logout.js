import React from 'react';

import {logout} from './actions/user';
import {handleSubmit} from '../inputHelpers';

function Logout({
  setUser,
  user,
}) {
  return (
    <form onSubmit={handleSubmit(setUser.bind(this, logout()))}>
      <label htmlFor="logout-submit">Logged in as: <strong>{user}</strong> </label>
      <input
        id="logout-submit"
        type="submit"
        value="Logout"
      />
    </form>
  )
}

export {Logout}
