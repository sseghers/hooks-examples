import React, { useEffect, useReducer } from 'react';

import {userReducer} from './user/reducers/user';
import {postReducer} from './post/reducers/post';
import {create} from './post/actions/post';
import {PostList} from './post/PostList';
import {CreatePost} from './post/CreatePost';
import {UserBar} from './user/UserBar';

const defaultPosts = [
  {
    author: 'Admin',
    content: 'First lol',
    id: 'uID1',
    title: 'First'
  },
  {
    author: 'Anon',
    content: 'Test Message',
    id: 'uID2',
    title: 'Foo'
  },
  {
    author: 'Super User',
    content: 'Super Message',
    id: 'uID3',
    title: 'Super Duper'
  }
];

const updatePosts = (handler, posts, action) =>
  post => handler(action({...post, id: `uID${posts.length + 1}`}));

function App() {
  const [userState, userDispatch] = useReducer(userReducer, '');
  const [postsState, postsDispatch] = useReducer(postReducer, defaultPosts);
  useEffect(() => {
    document.title = userState
      ? `${userState} - React Hooks Blog`
      : `React Hooks Blog`;
  }, [userState]);
  
  const createPost = userState && <CreatePost
    setPosts={updatePosts(postsDispatch, postsState, create)}
    user={userState} />;

  return (
    <div style={{padding: 8}}>
      <UserBar user={userState} setUser={userDispatch} />
      <br />
      {createPost}
      <br />
      <hr />
      <PostList posts={postsState} />
    </div>
  )
}

export {App}
