import React, { useContext, useEffect, useState } from 'react';
import { useResource } from 'react-request-hook';

import {StateContext} from '../contexts';
import {login} from './actions/user';
import {handleInput, handleSubmit} from '../inputHelpers';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginFailure, setLoginFailure] = useState(false);
  const {dispatch} = useContext(StateContext);
  const [user, loginUser] = useResource(({
    username,
    password,
  }) => ({
    url: `/login/${encodeURI(username)}/${encodeURI(password)}`,
    method: 'get',
  }));
  useEffect(() => {
    if(user.error) {
      setLoginFailure(true);
      return;
    }

    if(!user.data) {
      return;
    }

    const userData = user.data;
    if(!userData.length) {
      setLoginFailure(true);
      return;
    }
    
    setLoginFailure(false);
    dispatch(login({
      username: userData[0].username,
      password: userData[0].password,
    }));
  }, [user]);

  const handleUserLogin = () => {
    loginUser({ username, password });
  }

  const loginFailureMessage = loginFailure &&
    <span style={{ color: 'red'}}>Invalid username and|or password</span>;

  return (
    <form onSubmit={handleSubmit(handleUserLogin)}>
      <label htmlFor="login-username">Username:</label>
      <input
        onChange={handleInput(setUsername)}
        id="login-username"
        placeholder="Cool User"
        type="text"
        value={username}
      />
      <label htmlFor="login-password">Password:</label>
      <input
        id="login-password"
        onChange={handleInput(setPassword)}
        placeholder="secret words"
        type="text"
        value={password}
      />
      <input
        disabled={!Boolean(username)}
        type="submit"
        value="Login"
      />
      {loginFailureMessage}
    </form>
  )
}

export {Login}
