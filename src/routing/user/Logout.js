import React, { useContext } from 'react';

import {StateContext} from '../contexts';
import {getUserName} from './selectors/selectors';
import {logout} from './actions/user';
import {handleSubmit} from '../inputHelpers';

function Logout() {
  const {state, dispatch} = useContext(StateContext);
  const userName = getUserName(state);

  return (
    <form onSubmit={handleSubmit(dispatch.bind(this, logout()))}>
      <label htmlFor="logout-submit">Logged in as: <strong>{userName}</strong> </label>
      <input
        id="logout-submit"
        type="submit"
        value="Logout"
      />
    </form>
  )
}

export {Logout}

export default Logout
