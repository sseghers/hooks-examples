import {NAMESPACE} from '../actions/types';

export const getUserState = state => state[NAMESPACE];
export const getUserName = state => getUserState(state).username || '';
