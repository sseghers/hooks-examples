import React, { useEffect, useReducer, useState } from 'react';
import  { Router, View } from 'react-navi';
import { mount, route, createBrowserNavigation } from 'navi';

import { PostPage } from './pages/PostPage';
import {BlogThemeContext, StateContext} from './contexts';
import {create } from './post/actions/post';
import {errorReducer} from './error/reducers/error';
import {getUserName} from './user/selectors/selectors';
import {HeaderBar} from './pages/HeaderBar';
import {HomePage} from './pages/HomePage';
import {NAMESPACE as errorNamespace} from './error/actions/types';
import {NAMESPACE as postNamespace} from './post/actions/types';
import {NAMESPACE as userNamespace} from './user/actions/types';
import {postReducer} from './post/reducers/post';
import {THEMES} from './theme/themes';
import {userReducer} from './user/reducers/user';

const constructHeaderText = maybeUser => maybeUser
  ? `${maybeUser} - React Hooks Blog`
  : 'React Hooks Blog';

const dispatchReducers = dispatchers => action =>
  dispatchers.forEach((currDispatch) => {
    currDispatch(action);
  });

const routes = mount({
  '/': route({
    view: <HomePage />
  }),
  '/view/:id': route(req => ({
    view: <PostPage id={req.params.id} />
  }))
});

function App() {
  const [userState, userDispatch] = useReducer(userReducer, {});
  const [postsState, postsDispatch] = useReducer(postReducer, []);
  const [errorsState, errorsDispatch] = useReducer(errorReducer, '');
  const state = {
    [userNamespace]: userState,
    [postNamespace]: postsState,
    [errorNamespace]: errorsState,
  };
  const userName = getUserName(state);
  const dispatch = dispatchReducers([
    userDispatch,
    postsDispatch,
    errorsDispatch,
  ]);
  /* Theme Syncing */
  const [theme, setTheme] = useState(THEMES[0]);
  /* Update title */
  useEffect(() => {
    document.title = constructHeaderText(userName);
  }, [userName]);

  return (
    <StateContext.Provider value={{state, dispatch}}>
      <BlogThemeContext.Provider value={theme}>
        <Router routes={routes}>
          <div style={{padding: 8}}>
            <HeaderBar
              headerText={constructHeaderText(userName)}
              setTheme={setTheme}
              showHeader={userName}
              />
            <br />
            <hr />
            <View />
          </div>
        </Router>
      </BlogThemeContext.Provider>
    </StateContext.Provider>
  )
}

export {App}
