import React, {useContext} from 'react';

import {BlogThemeContext} from './contexts';

function Header({
  text,
}) {
  const {primaryColor} = useContext(BlogThemeContext);
  return (<h1 style={{color: primaryColor}}>{text}</h1>)
}

export {Header}
