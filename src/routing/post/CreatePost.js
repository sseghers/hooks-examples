import React, { useContext, useState } from 'react';

import {StateContext} from '../contexts';
import {getUserName} from '../user/selectors/selectors';
import { handleInput, handleSubmit } from '../inputHelpers';

function makePostHandler(
  content,
  setPosts,
  title,
  user,
) {
  return () => {
    setPosts({
      author: user,
      content,
      title,
    });
   
  }
}

function CreatePost({
  setPosts,
}) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const {state} = useContext(StateContext);
  const userName = getUserName(state);

  return (
    <form onSubmit={handleSubmit(makePostHandler(content, setPosts, title, userName))}>
      <div>Author: <strong>{userName}</strong></div>
      <br />
      <div>
        <label htmlFor="createPost-title">Title: </label>
        <input
          onChange={handleInput(setTitle)}
          id="createPost-title"
          type="text"
          value={title}
        />
      </div>
      <br />
      <textarea onChange={handleInput(setContent)} value={content} />
      <br />
      <input
        type="submit"
        value="Create"
        />
    </form>
  )
}

export {CreatePost}
