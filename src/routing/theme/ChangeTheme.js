import React, { useEffect } from 'react';
import { useResource } from 'react-request-hook';

import {ThemeItem} from './ThemeItem';

const isActive = (activeTheme, theme) =>
  activeTheme.primaryColor === theme.primaryColor
  && activeTheme.secondaryColor === theme.secondaryColor; 

function ChangeTheme({
  setTheme,
  theme,
}) {
  const [themes, getThemes] = useResource(() => ({
    url: '/themes',
    method: 'get',
  }));
  const {data: themesData, isLoading} = themes;
  useEffect(getThemes, []);

  if(isLoading) {
    return 'Loading themes...';
  }

  return (<div style={{paddingBottom: 16}}>
    <p>Change Theme:</p>
    {
      (themesData || []).map(currTheme => <ThemeItem
        active={isActive(theme, currTheme)}
        key={`${currTheme.primaryColor}:${currTheme.secondaryColor}`}
        onClick={setTheme}
        theme={currTheme}
      />)
    }
  </div>)
}

export {ChangeTheme}
