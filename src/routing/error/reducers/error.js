import * as errorTypes from '../actions/types';

const initialState = '';

function errorReducer(state = initialState, action) {
  switch(action.type) {
    case errorTypes.postError:
      return 'Failed to fetch posts. =(';    
    default:
      return state;
  }
}

export {errorReducer};
