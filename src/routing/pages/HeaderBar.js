import React, { useContext, useEffect } from 'react';
import  { useNavigation } from 'react-navi';
import { useResource } from 'react-request-hook';

import {BlogThemeContext} from '../contexts';
import {ChangeTheme} from '../theme/ChangeTheme';
import {CreatePost} from '../post/CreatePost';
import {getPostsState} from '../post/selectors/selectors';
import {Header} from '../Header';
import {StateContext} from '../contexts';
import {UserBar} from '../user/UserBar';

const updatePosts = (handler, posts) => {
  return post => {
    return handler({...post, id: `uID${posts.length + 1}`});
  }
}

const getPostHandler = sendPost => newPost => sendPost(newPost);

function HeaderBar({
  headerText,
  setTheme,
  showHeader,
}) {
  const navigation = useNavigation();
  const theme = useContext(BlogThemeContext);
  const {state} = useContext(StateContext);
  const postsState = getPostsState(state);
   /* Update posts */
   const [posts, sendPost] = useResource(postData => ({
    url: '/posts',
    method: 'post',
    data: postData,
  }));
  useEffect(() => {
    if(!posts.data || posts.isLoading || posts.data?.length >= 0) {
      return;
    }

    navigation.navigate(`/view/${posts.data.id}`);
  }, [posts]);


  const createPost = showHeader &&
    <CreatePost
      setPosts={updatePosts(getPostHandler(sendPost), postsState)}
    />;

  return (<div>
    <Header text={headerText} />
    <ChangeTheme setTheme={setTheme} theme={theme}/>
    <React.Suspense fallback={"Loading user area..."}>
      <UserBar />
    </React.Suspense>
    <br />
    {createPost}
  </div>)
}

export {HeaderBar}
