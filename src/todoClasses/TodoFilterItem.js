import React from 'react';

class TodoFilterItem extends React.Component {
  constructor(props) {
    super(props);

    this.handleFilter = this.handleFilter.bind(this);
  }

  handleFilter() {
    this.props.filterTodos(this.props.name)
  }

  render() {
    const {name, filter = 'all'} = this.props;
    const filterStyles = {
      color: 'blue',
      cursor: 'pointer',
      fontWeight: (filter === 'name' ? 'bold' : 'normal')
    }

    return <span
      style={filterStyles}
      onClick={this.handleFilter}
      >{name}</span>;
  }
}

export {TodoFilterItem}
