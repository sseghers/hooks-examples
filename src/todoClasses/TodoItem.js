import React from 'react';

class TodoItem extends React.Component {
  constructor(props) {
    super(props);

    this.handleRemove = this.handleRemove.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleRemove() {
    this.props.removeTodo(this.props.id);
  }

  handleToggle() {
    this.props.toggleTodo(this.props.id);
  }

  render() {
    const {
      completed,
      title,
    } = this.props;
    return (
      <div style={{width: 400, height: 25}}>
        <input
          checked={completed}
          onChange={this.handleToggle}
          type="checkbox"
        />
        {title}
        <button
          style={{float: 'right'}}
          onClick={this.handleRemove}
          >X</button>
      </div>
    );
  }
}

export {TodoItem}
