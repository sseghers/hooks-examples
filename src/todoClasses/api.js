export const generateUID = () => {
  const S4 = () => (((1+Math.random())*0x10000)|0).toString(16).substring(1);
  return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

export const fetchAPITodos = () => new Promise((res, rej) => {
  setTimeout(() => res([
    {id: generateUID(), title: 'Write react hooks book', completed: true},
    {id: generateUID(), title: 'Promote book', completed: false}
  ]), 100);
});
