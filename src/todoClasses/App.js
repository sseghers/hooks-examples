import React from 'react';

import {AddTodo} from './AddTodo';
import {Header} from './Header';
import {StateContext} from './StateContext';
import {TodoFilter} from './TodoFilter';
import {TodoList} from './TodoList';
import {generateUID, fetchAPITodos} from './api';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: 'all',
      filteredTodos: [],
      todos: [],
    };

    
  this.fetchTodos = this.fetchTodos.bind(this);
  this.addTodo = this.addTodo.bind(this);
  this.toggleTodo = this.toggleTodo.bind(this);
  this.removeTodo = this.removeTodo.bind(this);
  this.filterTodos = this.filterTodos.bind(this);
}

  componentDidMount() {
    this.fetchTodos();
  }

  addTodo(title) {
    const {todos} = this.state;
    
    this.setState({
      todos: [
        {
          completed: false,
          id: generateUID(),
          title,
        },
        ...todos,
        
      ]
    });
    this.filterTodos();
  }

  applyFilter(todos, filter) {
    switch(filter) {
      case 'active':
          return todos.filter(todo => !todo.completed);
      case 'completed':
        return todos.filter(todo => todo.completed);
      case 'all':
      default:
        return todos;
    }
  }

  fetchTodos() {
    fetchAPITodos()
      .then(todos => {
        this.setState({todos});
        this.filterTodos();
      });
  }

  filterTodos(filterArg) {
    this.setState(({filter, todos}) => ({
      filter: filterArg || filter,
      filteredTodos: this.applyFilter(todos, filterArg || filter),
    }));
  }

  toggleTodo(id) {
    const newTodos = this.state.todos.map(todo => {
      if(todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }
      return todo;
    });
    this.setState({todos: newTodos});
    this.filterTodos();
  }

  removeTodo(id) {
    const {todos} = this.state;
    const newTodos = todos.filter(todo => todo.id !== id);

    this.setState({todos: newTodos});
    this.filterTodos();
  }

  render() {
    const {filter, filteredTodos} = this.state;

    return (
      <StateContext.Provider value={filteredTodos}>
        <div style={{ width: 400 }}>
          <Header />
          <AddTodo addTodo={this.addTodo} />
          <hr />
          <TodoList toggleTodo={this.toggleTodo} removeTodo={this.removeTodo} />
          <hr />
          <TodoFilter filter={filter} filterTodos={this.filterTodos} />
        </div>
      </StateContext.Provider>
    );
  }
}

export {App}
