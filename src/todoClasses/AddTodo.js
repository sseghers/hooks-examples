import React from 'react';

import {handleInput} from './helpers';

class AddTodo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      input: '',
    }

    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    const {input} = this.state;
    const {addTodo} = this.props;

    if(input) {
      addTodo(input);
      this.setState({
        input: '',
      });
    }
  }

  render() {
    const {input} = this.state;

    // addTodo
    return (
      <form onSubmit={event => {
        event.preventDefault();
        this.handleAdd();
      }}>
        <input
          onChange={handleInput(input => this.setState({input}))}
          placeholder="Enter new task..."
          style={{width: 350, height: 15}}
          type="text"
          value={input}
        />
        <input
          disabled={!input}
          style={{float: 'right', marginTop: 2}}
          type="submit"
          value="Add"
        />
      </form>
    )
  }
}

export {AddTodo}
