import React, { useContext } from 'react';
import {useInput} from 'react-hookedup';

import {StateContext} from '../contexts';
import {getUserName} from '../user/selectors/selectors';
import { handleSubmit } from '../inputHelpers';

function makePostHandler(
  content,
  setPosts,
  title,
  user,
) {
  return () => {
    setPosts({
      author: user,
      content,
      title,
    });
   
  }
}

function CreatePost({
  setPosts,
}) {
  const {
    bindToInput: bindTitle,
    value: title,
  } = useInput('');
  const {
    bindToInput: bindContent,
    value: content,
  } = useInput('');
  const {state} = useContext(StateContext);
  const userName = getUserName(state);

  return (
    <form onSubmit={handleSubmit(makePostHandler(content, setPosts, title, userName))}>
      <div>Author: <strong>{userName}</strong></div>
      <br />
      <div>
        <label for="createPost-title">Title: </label>
        <input
          {...bindTitle}
          id="createPost-title"
          type="text"
          value={title}
        />
      </div>
      <br />
      <textarea {...bindContent} value={content} />
      <br />
      <input
        type="submit"
        value="Create"
        />
    </form>
  )
}

export {CreatePost}
