import {NAMESPACE} from '../actions/types';

export const getPostsState = state => state[NAMESPACE];
