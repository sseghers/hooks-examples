import React, { useContext, useEffect, useState } from 'react';
import { useResource } from 'react-request-hook';
import {useInput} from 'react-hookedup';

import {StateContext} from '../contexts';
import {login} from './actions/user';
import {handleSubmit} from '../inputHelpers';

function Login() {
  const {
    bindToInput: bindUsername,
    value: username,
  } = useInput('');
  const {
    bindToInput: bindPassword,
    value: password
  } = useInput('');
  const [loginFailure, setLoginFailure] = useState(false);
  const {dispatch} = useContext(StateContext);
  const [user, loginUser] = useResource(({
    username,
    password,
  }) => ({
    url: `/login/${encodeURI(username)}/${encodeURI(password)}`,
    method: 'get',
  }));
  useEffect(() => {
    if(user.error) {
      setLoginFailure(true);
      return;
    }

    if(!user.data) {
      return;
    }

    const userData = user.data;
    if(!userData.length) {
      setLoginFailure(true);
      return;
    }
    
    setLoginFailure(false);
    dispatch(login({
      username: userData[0].username,
      password: userData[0].password,
    }));
  }, [user]);

  const handleUserLogin = () => {
    loginUser({ username, password });
  }

  const loginFailureMessage = loginFailure &&
    <span style={{ color: 'red'}}>Invalid username and|or password</span>;

  return (
    <form onSubmit={handleSubmit(handleUserLogin)}>
      <label for="login-username">Username:</label>
      <input
        {...bindUsername}
        id="login-username"
        placeholder="Cool User"
        type="text"
        value={username}
      />
      <label for="login-password">Password:</label>
      <input
        {...bindPassword}
        id="login-password"
        placeholder="secret words"
        type="text"
        value={password}
      />
      <input
        disabled={!Boolean(username)}
        type="submit"
        value="Login"
      />
      {loginFailureMessage}
    </form>
  )
}

export {Login}
