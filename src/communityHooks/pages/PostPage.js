import React, { useEffect } from 'react';
import { useResource } from 'react-request-hook';
import {Link} from 'react-navi';

import {Post} from '../post/Post';
import {FooterBar} from '../pages/FooterBar';

function PostPage({id}) {
  const [post, getPost] = useResource(postId => ({
    url: `/posts/${postId}`,
    method: 'get',
  }));
  useEffect(() => {
    getPost(id)
  },[id]);
  
  const singlePost = post && post.data
    ? <Post {...post.data} />
    : 'Loading post...';

  return (<div>
    <div><Link href="/">Go back</Link></div>
    {singlePost}
    <hr />
    <FooterBar />
  </div>)
}

export {PostPage}
