export const THEMES = [
  { primaryColor: 'deepskyblue', secondaryColor: 'coral'},
  { primaryColor: 'orchid', secondaryColor: 'mediumseagreen'},
  { primaryColor: 'magenta', secondaryColor: 'blueviolet'}
];
