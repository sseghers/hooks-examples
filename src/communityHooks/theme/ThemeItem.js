import React from 'react';

function ThemeItem({
  active,
  onClick,
  theme,
}) {
  const itemStyles = {
    cursor: 'pointer',
    marginRight: 8,
    padding: 8,
    fontWeight: active ? 'bold' : 'normal',
  };

  return (
    <span onClick={() => onClick(theme)} style={itemStyles}>
      <span style={{color: theme.primaryColor}}>Primary</span>
      /
      <span style={{color: theme.secondaryColor}}>Secondary</span>
    </span>
  )
}

export {ThemeItem}
