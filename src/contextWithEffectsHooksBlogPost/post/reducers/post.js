import * as postTypes from '../actions/types';

const initialState = [];

function postReducer(state = initialState, action) {
  switch(action.type) {
    case postTypes.create:
      return [{...action.payload}, ...state];
    case postTypes.retrieve:
      return action.payload;
    default:
      return state;
  }
}

export {postReducer};
