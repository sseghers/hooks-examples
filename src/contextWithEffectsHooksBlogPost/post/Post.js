import React, { useContext } from 'react';

import {BlogThemeContext} from '../contexts';

const Post = React.memo(function ({
  author,
  content,
  title,
}) {
  const {secondaryColor} = useContext(BlogThemeContext);
  return (
    <div>
      <h3 style={{color: secondaryColor}}>{title}</h3>
      <div>{content}</div>
      <br />
      <em>Written by: <strong>{author}</strong></em>
    </div>
  )
})

export { Post }
