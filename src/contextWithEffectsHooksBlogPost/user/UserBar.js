import React, { useContext } from 'react';

import {StateContext} from '../contexts';
import {getUserName} from './selectors/selectors';
import {Login} from './Login';
import {Register} from './Register';

const Logout = React.lazy(() => import('./Logout'));

function UserBar() {
  const {state} = useContext(StateContext);
  const userName = getUserName(state);
  if(userName) {
    return <Logout />
  }

  return (
  <>
    <Login />
    <br />
    <Register />
  </>
  )
}

export {UserBar}
