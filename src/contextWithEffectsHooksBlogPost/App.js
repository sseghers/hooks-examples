import React, { useEffect, useReducer, useState } from 'react';
import { useResource } from 'react-request-hook';

import {userReducer} from './user/reducers/user';
import {NAMESPACE as userNamespace} from './user/actions/types';
import {getUserName} from './user/selectors/selectors';
import {postReducer} from './post/reducers/post';
import {create, retrieve } from './post/actions/post';
import {errorReducer} from './error/reducers/error';
import {NAMESPACE as errorNamespace} from './error/actions/types';
import {capturePostError} from './error/actions/errors';
import {NAMESPACE as postNamespace} from './post/actions/types';
import {PostList} from './post/PostList';
import {CreatePost} from './post/CreatePost';
import {UserBar} from './user/UserBar';
import {Header} from './Header';
import {BlogThemeContext, StateContext} from './contexts';
import {ChangeTheme} from './theme/ChangeTheme';
import {THEMES} from './theme/themes';


const updatePosts = (handler, posts) =>
  post => handler({...post, id: `uID${posts.length + 1}`});

const constructHeaderText = maybeUser => maybeUser
  ? `${maybeUser} - React Hooks Blog`
  : 'React Hooks Blog';

const dispatchReducers = dispatchers => action =>
  dispatchers.forEach((currDispatch) => {
    currDispatch(action);
  });


function App() {
  /*
    The idea of keeping the reducers and their states separate was alluring, but ultimately
    a hassle. I opted to see how much work would be involved in maintaining them individually while
    providing them via Context. 
    The truth is, the namespacing would already be happening within the one reducer, and I wouldn't
    need my own dispatch wrapper to ensure each reducer receives the action.
  */
  const [userState, userDispatch] = useReducer(userReducer, {});
  const [postsState, postsDispatch] = useReducer(postReducer, []);
  const [errorsState, errorsDispatch] = useReducer(errorReducer, '');
  const state = {
    [userNamespace]: userState,
    [postNamespace]: postsState,
    [errorNamespace]: errorsState,
  };
  const userName = getUserName(state);
  const dispatch = dispatchReducers([
    userDispatch,
    postsDispatch,
    errorsDispatch,
  ]);
  /* Theme Syncing */
  const [theme, setTheme] = useState(THEMES[0]);
  /* Update title */
  useEffect(() => {
    document.title = constructHeaderText(userName);
  }, [userName]);
  /* Fetch posts */
  const [posts, getPosts] = useResource(() => ({
    url: '/posts',
    method: 'get',
  }));
  useEffect(getPosts, []);
  useEffect(() => {
    if(posts && posts.error) {
      errorsDispatch(capturePostError(posts.error));
    }
    if(posts && posts.data) {
      postsDispatch(retrieve(posts.data.reverse()));
    }
  }, [posts]);
  /* Update posts */
  const [, sendPost] = useResource(postData => ({
    url: '/posts',
    method: 'post',
    data: postData,
  }));
  const postHandler = newPost => {
    sendPost(newPost);
    postsDispatch(create(newPost));
  }
  const createPost = userName && <CreatePost
    setPosts={updatePosts(postHandler, postsState)}
    />;

  let postsBlock = null;
  const postsError = errorsState;
  if(postsError) {
    postsBlock = <strong>{postsError}</strong>
  } else {
    postsBlock = <PostList />
  }

  /*
  Also, Holy ShIt that's a lot of code just for hooks. Granted the functions of the legacy
  lifecycle methods have consolidated to the "render" path, but now with effect hooks the concerns
  grow ever larger. useEffect is going to be where the services handling async things are called.
  */

  return (
    <StateContext.Provider value={{state, dispatch}}>
      <BlogThemeContext.Provider value={theme}>
        <div style={{padding: 8}}>
          <Header text={constructHeaderText(userName)} />
          <ChangeTheme setTheme={setTheme} theme={theme}/>
          <React.Suspense fallback={"Loading user area..."}>
            <UserBar />
          </React.Suspense>
          <br />
          {createPost}
          <br />
          <hr />
          {postsBlock}
        </div>
      </BlogThemeContext.Provider>
    </StateContext.Provider>
  )
}

export {App}
