import * as errorTypes from './types'; 

export const capturePostError = payload => ({
  payload,
  type: errorTypes.postError,
});
