import {NAMESPACE} from '../actions/types';

export const getErrorsState = state => state[NAMESPACE];
